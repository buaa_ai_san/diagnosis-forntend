import { upload, getImages, delImage } from '@/api/image'

const actions = {
  // upload image
  upload(commit, data) {
    console.log(data)
    return new Promise((resolve, reject) => {
      upload(data).then(response => {
        resolve(response)
      }).catch(error => {
        reject(error)
      })
    })
  },

  // list images
  getImages(commit, data) {
    return new Promise((resolve, reject) => {
      getImages(data).then(response => {
        resolve(response)
      }).catch(error => {
        reject(error)
      })
    })
  },
  // delete image
  delImage(commit, data) {
    return new Promise((resolve, reject) => {
      delImage(data).then(response => {
        resolve(response)
      }).catch(error => {
        reject(error)
      })
    })
  }
}

export default {
  namespaced: true,
  actions
}

