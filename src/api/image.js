import request from '@/utils/request'

export function upload(data) {
  console.log(data)
  return request({
    url: '/images',
    method: 'post',
    data
  })
}

export function getImages(params) {
  return request({
    url: '/images',
    method: 'get',
    params
  })
}

export function delImage(img_id) {
  return request({
    url: '/images/' + img_id,
    method: 'delete'
  })
}

