import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/* Layout */
import Layout from '@/layout'

/**
 * Note: sub-menu only appear when route children.length >= 1
 * Detail see: https://panjiachen.github.io/vue-element-admin-site/guide/essentials/router-and-nav.html
 *
 * hidden: true                   if set true, item will not show in the sidebar(default is false)
 * alwaysShow: true               if set true, will always show the root menu
 *                                if not set alwaysShow, when item has more than one children route,
 *                                it will becomes nested mode, otherwise not show the root menu
 * redirect: noRedirect           if set noRedirect will no redirect in the breadcrumb
 * name:'router-name'             the name is used by <keep-alive> (must set!!!)
 * meta : {
    roles: ['admin','editor']    control the page roles (you can set multiple roles)
    title: 'title'               the name show in sidebar and breadcrumb (recommend set)
    icon: 'svg-name'             the icon show in the sidebar
    breadcrumb: false            if set false, the item will hidden in breadcrumb(default is true)
    activeMenu: '/example/list'  if set path, the sidebar will highlight the path you set
  }
 */

/**
 * constantRoutes
 * a base page that does not have permission requirements
 * all roles can be accessed
 */
export const constantRoutes = [
  {
    path: '/login',
    component: () => import('@/views/login/index'),
    hidden: true
  },

  {
    path: '/',
    component: Layout,
    redirect: '/dashboard',
    children: [{
      path: 'dashboard',
      name: 'Dashboard',
      component: () => import('@/views/dashboard/index'),
      meta: { title: '通知说明', icon: 'dashboard' }
    }]
  },

  {
    path: '/404',
    component: () => import('@/views/404'),
    hidden: true
  }

]

/**
 * asyncRoutes
 * the routes that need to be dynamically loaded based on user roles
 */
export const asyncRoutes = [

  {
    path: '/patient',
    component: Layout,
    children: [
      {
        path: 'index',
        name: 'Patient',
        component: () => import('@/views/patient/index'),
        meta: {
          title: '患者管理',
          icon: 'user',
          roles: ['admin', 'register']
        }
      }
    ]
  },

  {
    path: '/photo',
    component: Layout,
    children: [
      {
        path: 'index',
        name: 'Photo',
        component: () => import('@/views/photo/index'),
        meta: {
          title: '医学影像管理',
          icon: 'eye-open',
          roles: ['admin', 'radio']
        }
      }
    ]
  },

  {
    path: '/diagnosis',
    component: Layout,
    children: [
      {
        path: 'index',
        name: 'Diagnosis',
        component: () => import('@/views/diagnosis/index'),
        meta: {
          title: '影像结果',
          icon: 'form',
          roles: ['admin', 'onco']
        }
      }
    ]
  },

  {
    path: '/feedback',
    component: Layout,
    children: [
      {
        path: 'index',
        name: 'Feedback',
        component: () => import('@/views/feedback/index'),
        meta: {
          title: '故障反馈',
          icon: 'bug'
        }
      }
    ]
  },

  // 404 page must be placed at the end !!!
  { path: '*', redirect: '/404', hidden: true }
]

const createRouter = () => new Router({
  // mode: 'history', // require service support
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRoutes
})

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}

export default router
