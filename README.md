### 诊断前端

用于展示诊断流程

### 参考

- [vue-element-admin](https://panjiachen.gitee.io/vue-element-admin-site/zh/)
- [ElementUI](https://element.eleme.io/#/zh-CN/component)
- [Vue Router](https://router.vuejs.org/zh/guide/)

### Build Setup


```bash
# clone the project
git clone git@gitlab.com:buaa_ai_san/diagnosis-forntend.git

# enter the project directory
cd diagnosis-forntend

# install dependency
npm install

# develop
npm run dev
```

This will automatically open http://localhost:9528

### Build

```bash
# build for test environment
npm run build:stage

# build for production environment
npm run build:prod
```

### Advanced

```bash
# preview the release environment effect
npm run preview

# preview the release environment effect + static resource analysis
npm run preview -- --report

# code format check
npm run lint

# code format check and auto fix
npm run lint -- --fix
```